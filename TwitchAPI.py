#!/usr/bin/env python
# -*- coding: utf-8 -*-

from twitchAPI.twitch import Twitch

from spike_settings.SpikeSettings import SpikeSettings


class TwitchAPI:
    def __init__(self):
        self.twitch = Twitch(SpikeSettings.get_twitch_client_id(), SpikeSettings.get_twitch_secret())
        self.twitch.authenticate_app([])

    def get_live_channel(self, channel_id: int):
        try:
            rep = self.twitch.get_streams(user_id=[str(channel_id)])
        except:
            rep = {}
        return rep

    def get_blood_bowl_live(self, language=None):
        try:
            rep = self.twitch.get_streams(game_id="490177", language=language)
        except:
            rep = {}
        return rep

    def get_user_info(self, user_name: str):
        try:
            rep = self.twitch.get_users(logins=[user_name])
        except:
            rep = {}
        return rep

    def get_game_info(self, game_id: int):
        try:
            rep = self.twitch.get_games(game_ids=[str(game_id)])
        except:
            rep = {}
        return rep