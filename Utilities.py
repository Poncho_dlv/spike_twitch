#!/usr/bin/env python
# -*- coding: utf-8 -*-

from spike_database.TwitchChannel import ChannelStatus
from spike_database.TwitchChannel import TwitchChannel
from spike_twitch.TwitchAPI import TwitchAPI


class Utilities:

    @staticmethod
    def get_new_lives():
        stream_live = []
        try:
            twitch_api = TwitchAPI()
            twitch_db = TwitchChannel()
            twitch_channel_ids = twitch_db.get_registered_channel()

            for channel in twitch_channel_ids:
                twitch_data = twitch_api.get_live_channel(channel.get("channel_id"))
                if len(twitch_data.get("data", [])) == 1:
                    twitch_data = twitch_data["data"][0]
                    if channel.get("status", ChannelStatus.OFFLINE) == ChannelStatus.OFFLINE and channel.get("started_at") != twitch_data.get("started_at"):
                        twitch_data["discord_guild"] = channel.get("discord_guild", [])
                        stream_live.append(twitch_data)
                        twitch_db.set_channel_status(channel.get("channel_id"), ChannelStatus.LIVE, twitch_data.get("started_at"))
                    elif channel.get("started_at") == twitch_data.get("started_at"):
                        twitch_db.set_channel_status(channel.get("channel_id"), ChannelStatus.LIVE)
                else:
                    twitch_db.set_channel_status(channel.get("channel_id"), ChannelStatus.OFFLINE)
        except:
            pass
        return stream_live

    @staticmethod
    def user_exist(user_name: str):
        try:
            twitch_api = TwitchAPI()
            data = twitch_api.get_user_info(user_name)
            if len(data.get("data", [])) > 0:
                return True, data.get("data", [])
        except:
            pass
        return False, None

    @staticmethod
    def get_game_name(game_id: int):
        try:
            twitch_api = TwitchAPI()
            data = twitch_api.get_game_info(game_id)
            return data.get("data", [{}])[0].get("name")
        except:
            return None
